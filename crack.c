#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash. 
// That is, return 1 if the guess is correct.

int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *g = md5(guess, strlen(guess));

    // Compare the two hashes
    if( strcmp(hash, g) == 0)
    { 
        free(g);
        return 1;
    }
    else
    {
        free(g);
        return 0;
    }
}
// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.
char **read_dictionary(char *filename, int *size)
{
    struct stat info;
    if (stat(filename, &info) == -1)
    {
        fprintf(stderr, "Can't get info about %s\n", filename);
        exit(1);
    }
    
    *size = info.st_size;
    
    FILE *f;
    f = fopen(filename, "r");
    if (!f)
    {
        printf("Can not open file"); 
    }
    
    char *d = malloc(info.st_size);
    fread(d, sizeof(char), info.st_size, f);
    fclose(f);
    
    int num = 0;
    for(int i = 0; i < info.st_size; i++)
    {
        if(d[i] == '\n')
        {
            d[i] = '\0';
            num++;
        }
    }
    char **dictionary = malloc(num * sizeof(char*));
    dictionary[0] = &d[0];
    int j = 1;
    for(int i =0; i < info.st_size - 1; i++)  
    {
        if(d[i] == '\0')
        {
            dictionary[j] = &d[i+1];
            j++;
        }
    }
    free(dictionary);
    free(d);
    return dictionary;
} 
int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    // Read the dictionary file into an array of strings.
    int dlen;
    char **dict = read_dictionary(argv[2], &dlen); 
    
    //Open the hash file for reading. 
    FILE *fp = fopen("hashes.txt", "r");
    if(!fp)
    {
        printf("Can't open file for reading\n");
        exit(1);
    }
    char *h = malloc(HASH_LEN);
    
    while(fgets(h, HASH_LEN, fp) != NULL)
    {
        printf("Hash: %s", h);
        // For each hash, try every entry in the dictionary.
        for(int j = 0; j < dlen; j++) 
        {
            if( tryguess(h, dict[j]) == 1)
            {
                printf(" Word: %s\n", dict[j]);
                //break;
            }
        }
    }
    fclose(fp);
    free(h);
    // Print the matching dictionary entry.
    // Need two nested loops.
    
}
